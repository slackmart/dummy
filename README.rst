Super Trello
============


::

    $ python3 -m venv venv-for-trello
    $ . venv-for-trello/bin/activate
    (venv-for-trello) $ pip install -r requirements.txt
    (venv-for-trello) $ source .envrc && python say_hello.py
    (venv-for-trello) $ source .envrc && python butters.py
