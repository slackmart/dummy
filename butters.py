import os
import requests
import datetime

from telegram.ext import CommandHandler, Updater


def parse_message(message):
    _, card, *concept, amount = message.text.split()
    print(concept, amount, card)
    m = {
        'cash': 1,
    }
    return {
        'source': m['cash'], 'concept': ' '.join(concept),
        'amount': amount, 'date': str(datetime.datetime.now().date())
    }


def new_handler(bot, update):
    payload = parse_message(update.message)
    response = requests.post(
        'http://localhost:8000/api/transactions/',
        headers={'Content-Type': 'application/json'},
        json=payload
    )
    if response.ok:
        update.message.reply_text(
            f'Transaction saved. Status {response.status_code}')
    else:
        update.message.reply_text(
            f'Error. Status {response.status_code}')


def main():
    token = os.getenv('BUTTERS_BOT_TOKEN')

    updater = Updater(token)
    dp = updater.dispatcher
    dp.add_handler(CommandHandler("new", new_handler))

    # Start the Bot
    updater.start_polling(timeout=10)

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT.
    updater.idle()


if __name__ == '__main__':
    main()
